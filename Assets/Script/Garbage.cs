﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Garbage : MonoBehaviour
{
    enum Side
    {
        Left,
        Right
    }
    [SerializeField]
    private Side sideGarbage;

    

    void AddPoints(Collider other)
    {
        other.gameObject.SetActive(false);
        Static.AddPoints();
    }

    void SubtractPoints(Collider other)
    {
        Static.Score -= 10;
        Static.DecreaseLife();
        Static.EventLifeUpdate();
        Static.EventScoreUpdate();
        other.gameObject.SetActive(false);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (sideGarbage == Side.Left)
        {
            if (other.CompareTag("Left"))
            {
                AddPoints(other);
            }
            if (other.CompareTag("Right"))
            {
                SubtractPoints(other);
            }
           
        }

        if (sideGarbage == Side.Right)
        {
            if (other.CompareTag("Left"))
            {
                SubtractPoints(other);
            }
            if (other.CompareTag("Right"))
            {
                
                AddPoints(other);
            }
           
        }

       
    }
}
