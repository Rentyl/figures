﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    public static Spawner instance;
    private List<Figures> activeFigureList = new List<Figures>();
    private List<Figures> figuresList = new List<Figures>();
    private List<GameObject> figures = new List<GameObject>();
    [SerializeField]
    private GameObject[] allFigures;

    [SerializeField]
    private float timeSpawn = 1;
    private float timerSpawn;

    private int indexActiveFigure;
    private int randomRangeFigures;
    [SerializeField] 
    private Transform pointForSpawn;
    private Vector3 pointSpawn;
    [SerializeField]
    private GameObject figuresContainer;
    private float offsetBetweenFigures = 2f;


    void Awake()
    {
        instance = this;
       
        pointSpawn = pointForSpawn.position;
        RandomFigures();
        foreach (var v in figures)
        {
            figuresList.Add(v.GetComponent<Figures>());
            activeFigureList.Add(v.GetComponent<Figures>());
        }

        foreach (var v in figuresList)
        {
            v.activeFigures = false;
        }
        figuresList[0].activeFigures = true;
        figuresList[figures.Count - 1].isLastFigure = true;
    }

    List<GameObject> example  = new List<GameObject>();
    private void RandomFigures()
    {
        // Change size object 
        foreach (var v in allFigures)
        {
            v.transform.localScale = v.transform.localScale * 0.833f;
        }


        List<int> allFiguresForRandom = new List<int>();
        GameObject removedObject = new GameObject();
        // Кол-во фигур которые будут созданы фабрикой
        // Пройденный уровень увеличивает общее кол-во фигур
        int countFigures = Static.figuresOnLevel;
        // Узнаем общее кол-во фигур для рандомизатора
        int countOpenFigures = Static.openedFigures;
       
        int chanceFigure = 100 / countOpenFigures;
        
        int ceil = 0;
        for (int i = 0; i < countFigures; i++)
        {

            
            randomRangeFigures = Random.Range(0, 100);
            ceil = (int)Mathf.Ceil(randomRangeFigures / chanceFigure);
            allFiguresForRandom.Add(ceil);

            //print(ceil + "приведённое значение");
            figures.Add(Instantiate(allFigures[ceil],
                new Vector3(pointSpawn.x, pointSpawn.y + i * offsetBetweenFigures, pointSpawn.z),
                Quaternion.identity,
                figuresContainer.transform));
            // Удаляем выпавший объект из рандома

        }

    }

    void FindCopy(List<int> intList)
    {
        List<int> listForSort = new List<int>();
        int temp = 0;
        int repeatCount = 3;
        for (int i = 0; i < intList.Count;)
        {
            if (i + repeatCount < intList.Count)
            {
                if (intList.GetRange(i, i + repeatCount).Sum() / repeatCount == repeatCount)
                {
                }
            }
            
        }
    }
    // to raise the figures to point spawn
    public IEnumerator RaiseActiveFigures()
    {
       
        Static.Speed = -10;
        Static.EventUpdateFiguresSpeed();
        GameObject obj= figures.Find(x => x.activeInHierarchy);
        while (obj.transform.position.y < pointSpawn.y)
        {

            yield return null;
        }
        Static.Speed = Static.speedBeforeDefeat;
        Static.speedBeforeDefeat = 0;
        Static.EventUpdateFiguresSpeed();
        HideUsedFigure();

    }

    void OnEnable()
    {
        Static.ChangeActiveFigure += FindActiveFigure;
    }
    void OnDisable()
    {
        Static.ChangeActiveFigure -= FindActiveFigure;
    }

    void UpdateSpeedByTimer()
    {
        timerSpawn += Time.deltaTime;
        if (timerSpawn > timeSpawn)
        {
            timerSpawn = 0;
            Static.Speed += Static.speedModifier;
            Static.EventUpdateFiguresSpeed();
        }
    }

    void FindActiveFigure()
    {
        if (indexActiveFigure + 1 < activeFigureList.Count)
        {
            indexActiveFigure++;

        }
        else
        {
            indexActiveFigure = 0;
        }

        activeFigureList[indexActiveFigure].activeFigures = true;
    }


    public void HideUsedFigure()
    {
        foreach (var v in activeFigureList)
        {
            if (v.blockSwipe && v.isActiveAndEnabled)
            {
                v.gameObject.SetActive(false);
               
                Static.AddPoints();
                if (v.isLastFigure)
                {
                    Static.EventLevelIsComplete();
                }
            }
        }
    }


    void Update()
    {
        if(!Static.pause)
            UpdateSpeedByTimer();
    }
}
