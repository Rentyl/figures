﻿

using UnityEngine;


public class Figures : MonoBehaviour
{
    public Rigidbody rd;

    private Vector2 speedDirection;
    
    public bool activeFigures;

    public bool blockSwipe;
    public bool isLastFigure;
    private float speedRotation = 2;
    readonly float xOffsetFigures = 2;

    void Start()
    {
        rd = GetComponent<Rigidbody>();
        Static.Speed = Static.standardSpeedFigures;
        speedDirection = new Vector2(0, -Static.Speed);
    }

    void Falling()
    {

        rd.velocity = speedDirection;
    }

    void Update()
    {
        Falling();
        Rot();
    }

    void Rot()
    {
        rd.MoveRotation(Quaternion.Euler(new Vector3(speedRotation, 0, speedRotation)) * rd.rotation);
    }

    void OnEnable()
    {
        
        Static.SwipeLeft += SwipeLeft;
        Static.SwipeRight += SwipeRight;
        Static.UpdateFiguresSpeed += UpdateSpeed;
    }
    void OnDisable()
    {
        Static.SwipeLeft -= SwipeLeft;
        Static.SwipeRight -= SwipeRight;
        Static.UpdateFiguresSpeed -= UpdateSpeed;
    }

    void UpdateSpeed()
    {
        
        speedDirection.y = -Static.Speed;
    }


    void SwipeLeft()
    {
        if (!blockSwipe && activeFigures)
        {
            rd.position = new Vector3(-xOffsetFigures, rd.position.y, rd.position.z);
            blockSwipe = true;
            activeFigures = false;
            
        }
    }

    void SwipeRight()
    {
        if (!blockSwipe && activeFigures)
        {
            rd.position = new Vector3(xOffsetFigures, rd.position.y, rd.position.z);
            blockSwipe = true;
            activeFigures = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        activeFigures = false;
        if (other.CompareTag("Floor"))
        {
            
            Static.DecreaseLife();
            Static.EventLifeUpdate();

            blockSwipe = false;
            gameObject.SetActive(false);
            Static.EventChangeActiveFigure();

        }
        if (Static.CurrentLife > 0 && isLastFigure)
        {
            Static.EventLevelIsComplete();
        }
    }
}
