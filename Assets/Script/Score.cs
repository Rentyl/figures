﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI scoreText;
    void OnEnable()
    {
        Static.ScoreUpdate += UpdateScore;
    }
    void OnDisable()
    {
        Static.ScoreUpdate -= UpdateScore;
    }
    void UpdateScore()
    {
        scoreText.text = Static.Score.ToString();
    }
}
