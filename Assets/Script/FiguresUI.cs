﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FiguresUI : MonoBehaviour
{


    [SerializeField]
    private TextMeshProUGUI allFigures;
    [SerializeField]
    private TextMeshProUGUI collectedFigures;

    void Start()
    {
        allFigures.text = Static.figuresOnLevel.ToString();
    }

    void OnEnable()
    {
        Static.FigureIsDisable += UpdateFigureCounter;
    }

    void OnDisable()
    {
        Static.FigureIsDisable -= UpdateFigureCounter;
    }

    void UpdateFigureCounter()
    {
        if (Static.figuresOnLevel >= Static.collectedFigures + 1)
            Static.collectedFigures += 1;

        collectedFigures.text = Static.collectedFigures.ToString();
        
        
    }

}
