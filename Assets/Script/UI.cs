﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;
using GameAnalyticsSDK.Events;
using GameAnalyticsSDK.State;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;

public class UI : MonoBehaviour
{

    [SerializeField] 
    private Image[] lifeImage;
    [SerializeField]
    private Sprite lifeRed;
    [SerializeField]
    private Sprite lifeShadow;

    [SerializeField]
    private Transform shadowBackground;
    [SerializeField] 
    private Transform victory;

    [SerializeField]
    private Transform defeat;



    [SerializeField]
    Image adsWhite;
    [SerializeField]
    Image adsBlack;
    [SerializeField]
    Button adsButton;

    void Awake()
    {
       
       GameAnalytics.Initialize();

    }

    void Start()
    {
        if (GAState.IsManualSessionHandlingEnabled())
        {
            GameAnalytics.StartSession();
        }
        
        Static.pause = false;
        GameAnalytics.NewResourceEvent(GAResourceFlowType.Undefined, "currentLife", Static.CurrentLife, "CountLife", "CurrentLife");
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "LevelComplete");
    }

    void OnApplicationQuit()
    {
        if (GameAnalyticsSDK.State.GAState.IsManualSessionHandlingEnabled())
        {
            GameAnalytics.EndSession();
        }
    }
    
    void Update()
    {
    }

    void OnEnable()
    {

        Static.LifeUpdate += CheckAndSetLife;
        Static.LevelIsComplete += Victory;
    }
    void OnDisable()
    {
        Static.LifeUpdate -= CheckAndSetLife;
        Static.LevelIsComplete -= Victory;
    }

    void CheckAndSetLife()
    {
        switch (Static.CurrentLife)
        {
            case 0:
                foreach (var v in lifeImage)
                {
                    v.sprite = lifeShadow;
                    Defeat();
                }

                break;
            case 1:
                foreach (var v in lifeImage)
                {
                    v.sprite = lifeShadow;
                }

                lifeImage[0].sprite = lifeRed;

                break;
            case 2:
                foreach (var v in lifeImage)
                {
                    v.sprite = lifeRed;
                }

                lifeImage[2].sprite = lifeShadow;
                break;
            case 3:
                foreach (var v in lifeImage)
                {
                    v.sprite = lifeRed;
                }
                break;
        }
    }
    public void Revive()
    {
        Static.Revive();
        shadowBackground.gameObject.SetActive(false);
        defeat.gameObject.SetActive(false);
    }
    public void Restart()
    {
        Static.ResetAllData();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
       
    }
    void Defeat()
    {
        Static.Defeat();
        shadowBackground.gameObject.SetActive(true);
        defeat.gameObject.SetActive(true);
        
        
        StartCoroutine(AdsButtonTimer());
    }

    void Victory()
    {
        
        GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "currentLife", Static.CurrentLife, "CountLife", "CurrentLife");
        FacebookManagar.FbCompleteMission();
        Static.Victory();
        victory.gameObject.SetActive(true);
        shadowBackground.gameObject.SetActive(true);
    }

    IEnumerator AdsButtonTimer()
    {
        float timer = 0;
        while (timer < 1)
        {
            timer += Time.deltaTime * 0.3f;
            adsWhite.fillAmount = Mathf.Lerp(1, 0, timer);
            yield return null;
        }
        adsBlack.gameObject.SetActive(false);
        adsWhite.gameObject.SetActive(false);
        adsButton.gameObject.SetActive(false);
    }

}
