﻿

public class Static
{
    static int score;
    public static readonly float standardSpeedFigures = 2;
    static float speedFigures = 2;

    // force of increasing speed
    public static readonly float speedModifier = 1.5f;
    // speed taking into the increase
    public static float speedBeforeDefeat;


    public static int openedFigures = 5;
    public static int collectedFigures;
    public static readonly int figuresOnLevel = 50;
    
    static int currentLife = 3;


    public static bool pause;
    public static bool activeContr;

    public delegate void LeftSwipe();
    public delegate void RightSwipe();
    public delegate void UpdateScore();
    public delegate void UpdateLife();
    public delegate void UpdateSpeed();
    public delegate void FigureDisable();
    public delegate void ChangeActiveFigures();
    public delegate void LevelComplete();
    public delegate void LevelDefeat();


    public static event LevelDefeat LevelIsDefeat;
    public static event LevelComplete LevelIsComplete;
    public static event LeftSwipe SwipeLeft;
    public static event RightSwipe SwipeRight;
    public static event UpdateScore ScoreUpdate;
    public static event UpdateLife LifeUpdate;
    public static event UpdateSpeed UpdateFiguresSpeed;
    public static event FigureDisable FigureIsDisable;
    public static event ChangeActiveFigures ChangeActiveFigure;


    public static int Score
    {
        get
        {
            return score;
        }
        set
        {
            if (value >= 0)
            {
                score = value;
            }
            
        }
    }

    public static int CurrentLife
    {
        get { return currentLife; }

    }
    
    public static float Speed
    {
        get
        {
            return speedFigures;
        }
        set
        {
            if (value < 7)
            {
                speedFigures = value;
            }
            else
            {
                speedFigures = 7;
            }
        }
    }



    public static void EventLevelIsComplete()
    {
        if (LevelIsComplete != null)
            LevelIsComplete();

    }


    public static void EventLevelIsDefeat()
    {
        if (LevelIsDefeat != null)
            LevelIsDefeat();

    }

    public static void EventRightSwipe()
    {
        if (SwipeRight != null)
            SwipeRight();

    }

    public static void EventLeftSwipe()
    {
        if (SwipeLeft != null)
            SwipeLeft();
    }

    public static void EventScoreUpdate()
    {
        if (ScoreUpdate != null)
            ScoreUpdate();
    }
    public static void EventLifeUpdate()
    {
        if (LifeUpdate != null)
            LifeUpdate();
    }
    public static void EventUpdateFiguresSpeed()
    {
        if (UpdateFiguresSpeed != null)
            UpdateFiguresSpeed();
    }

    public static void EventFigureIsDisable()
    {
        if (FigureIsDisable != null)
            FigureIsDisable();
    }

    public static void EventChangeActiveFigure()
    {
        if (ChangeActiveFigure != null)
            ChangeActiveFigure();
    }

    public static void ResetAllData()
    {
        speedFigures = standardSpeedFigures;
        currentLife = 3;
        collectedFigures = 0;

    }


    public static void Victory()
    {
        pause = true;
        Speed = 0;
        EventUpdateFiguresSpeed();
    }

    public static void Defeat()
    {
        pause = true;
        if(Speed > 0)
            speedBeforeDefeat = Speed;
        Speed = 0;
        EventUpdateFiguresSpeed();

    }

    public static void Revive()
    {
        Spawner.instance.StartCoroutine(Spawner.instance.RaiseActiveFigures());
        pause = false;
        currentLife = 1;
        EventUpdateFiguresSpeed();
        EventLifeUpdate();
    }

    public static void DecreaseLife()
    {
        currentLife--;
    }

    public static void AddPoints()
    {
        Score += 10;
        EventScoreUpdate();
        EventFigureIsDisable();
    }
}
