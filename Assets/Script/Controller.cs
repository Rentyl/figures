﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;

public class Controller : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{

    Vector2 startSlide = Vector2.one;
    Vector2 endSlide = Vector2.one;

    Vector2 slideDist;
    float magn;
    Vector2 head;

    private bool isSlide;




    void CalculateVectors()
    {

        head = endSlide - startSlide;
        magn = head.magnitude;

        slideDist = head / magn;
    }

    public void OnPointerDown(PointerEventData eventData)
    {

        if (!Static.pause)
        {
            Static.activeContr = true;
            startSlide = Input.mousePosition;

        }


    }

    void ResetMovementVariable()
    {
        startSlide = endSlide;

    }


    public void OnPointerUp(PointerEventData eventData)
    {

        if (!Static.pause && isSlide)
        {
            ResetMovementVariable();
            Static.activeContr = false;
            isSlide = false;
            Static.EventChangeActiveFigure();
        }
    }


    public void OnDrag(PointerEventData eventData)
    {
        
        //конечная точка слайда
        endSlide = Input.mousePosition;
        if (!Static.pause && Static.activeContr)
        {
            CalculateVectors();
           
            if (slideDist.x > 0)
            {
                Static.EventRightSwipe();
                Static.activeContr = false;
                isSlide = true;
                return;
            }
            if(slideDist.x < 0)
            {
                Static.EventLeftSwipe();
                Static.activeContr = false;
                isSlide = true;

            }
            
        }
    }
}
